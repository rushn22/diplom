import requests
import json
import m3u8
import subprocess as sp
import FFredact

class Ustream:

    video_name = ""

    #получаем список
    def run(url):
        bad_link = Ustream.get_link(url)
        link = Ustream.normalize_link(bad_link)
        m3u8file = Ustream.get_m3u8(link)
        if not m3u8file:
            return False
        else:
            return Ustream.get_list(m3u8file)

#http://www.ustream.tv/decoraheagles
    #получаем ссылку на m3u8
    def get_link(url):
        if (url.find("embed/")>0):
            pos = url.find("embed/")+6
            video_id = url[pos:]
            pos1 = video_id.find("?")
            video_id = video_id[:pos1]
            link = "http://iphone-streaming.ustream.tv/uhls/"+video_id+"/streams/live/iphone/playlist.m3u8"
            Ustream.video_name = video_id
            return link
        Ustream.video_name = url[url.find(".tv/") + 4:]
        print(Ustream.video_name)
        responce = requests.get(url).text
        pos = responce.find('"stream":{"hls":') + 17
        pos1 = pos + 120
        link = responce[pos:pos1]
        pos1 = link.find('m3u8') +4
        link = link[:pos1]
        return link

    #приводим ссылку к нормальному виду
    def normalize_link(link):
        link = link.replace("\\", "")
        print(link)
        return link

    #получаем m3u8
    def get_m3u8(link):
        try:
            responce = requests.get(link).text
        except:
            return False
        m3u8_obj = m3u8.loads(responce)
        return m3u8_obj

    def get_list(m3u8_obj):
        list = {}
        for p in m3u8_obj.playlists:
            if (str(p.stream_info.resolution) == "(1920, 1080)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Ustream.video_name + " : " + "STREAM" + " : " + "1080"] = p.uri
            if (str(p.stream_info.resolution) == "(1280, 720)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Ustream.video_name + " : " + "STREAM" + " : " + "720"] = p.uri
            if (str(p.stream_info.resolution) == "(854, 480)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Ustream.video_name + " : " + "STREAM" + " : " + "480"] = p.uri
            if (str(p.stream_info.resolution) == "(640, 360)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Ustream.video_name + " : " + "STREAM" + " : " + "360"] = p.uri
            if (str(p.stream_info.resolution) == "(426, 240)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Ustream.video_name + " : " + "STREAM" + " : " + "240"] = p.uri
        #print(list)
        return list

    def get_json(links):
        dump = json.dumps(links)
        json_doc = json.loads(dump)
        return (json_doc)
