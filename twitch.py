import requests
import json
import m3u8
#from ffmpy import FFmpeg
import subprocess as sp
import FFredact
class Twitch:
    token = ""
    sig = ""
    client_id = "5w5861mvyntazbhrk0xkjb803t9obk"
    usher_api = "http://usher.twitch.tv/api/channel/hls/{channel}.m3u8?" + "player=twitchweb&&token={token}&sig={sig}&" + "allow_audio_only=true&allow_source=true&type=any&p=9371281'"
    token_api = "https://api.twitch.tv/api/channels/{channel}/access_token?client_id=5w5861mvyntazbhrk0xkjb803t9obk"
    channel_name = ""

    def run(url):
        Twitch.get_name(url)
        data = Twitch.get_token("")
        if not data:
            return False
        obj = Twitch.get_m3u8("")
        return(Twitch.get_list(obj))

    #получаем имя канала
    def get_name(url):
        if(url.find("player.twitch.tv")>0):
            pos = url.find("channel=")+8
            Twitch.channel_name = url[pos:].strip()
            return
        pos = url.find(".tv/")+4
        Twitch.channel_name = url[pos:].strip()
        #print(Twitch.channel_name)

    #получаем токен и sig
    def get_token(self):
        Twitch.token_api = Twitch.token_api.replace("{channel}",Twitch.channel_name)
        json_resp = requests.get(Twitch.token_api).json()
        try:
            Twitch.sig = json_resp['sig']
        except:
            return False
        Twitch.token = json_resp['token']
        return True

    #через usher получаем m3u8
    def get_m3u8(self):
        Twitch.usher_api = Twitch.usher_api.replace("{channel}",Twitch.channel_name).replace("{token}", Twitch.token).replace("{sig}", Twitch.sig)
        responce = requests.get(Twitch.usher_api).text
        #print(Twitch.usher_api)
        #print(responce)
        m3u8_obj = m3u8.loads(responce)
        return m3u8_obj

    #m3u8 в список
    def get_list(m3u8_obj):
        list = {}
        infos = ""
        for p in m3u8_obj.playlists:
            if (str(p.stream_info.resolution) == "(1920, 1080)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Twitch.channel_name + " : " + "STREAM" + " : "+ "1080"]=p.uri
            if (str(p.stream_info.resolution) == "(1280, 720)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Twitch.channel_name + " : " + "STREAM" + " : " + "720"] = p.uri
            if (str(p.stream_info.resolution) == "(852, 480)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Twitch.channel_name + " : " + "STREAM" + " : " + "480"] = p.uri
                #ff = FFmpeg(inputs={p.uri: None})
                #ff.run()
            if (str(p.stream_info.resolution) == "(640, 360)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Twitch.channel_name + " : " + "STREAM" + " : " + "360"] = p.uri
            if (str(p.stream_info.resolution) == "(400, 226)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Twitch.channel_name + " : " + "STREAM" + " : " + "225"] = p.uri
            if (str(p.stream_info.resolution) == "None"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Twitch.channel_name + " : " + "STREAM" + " : " + "Audio"] = p.uri
        #print(list)
        return list

    def get_json(links):
        dump = json.dumps(links)
        json_doc = json.loads(dump)
        return (json_doc)