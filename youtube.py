import requests
import urllib
import urllib.parse
import json
import m3u8
import FFredact

class Youtube:

    video_id = ""

    def run(url):
        Youtube.video_id = Youtube.find_id(url)
        urls = Youtube.find_video_url(Youtube.video_id)
        if (urls == "empty"):
            list = Youtube.live_video(Youtube.video_id)
            if list == False:
                return False
            else:
                return list
        else:
            video_info = Youtube.dec_urls(urls)
            links = Youtube.get_links_list(video_info)
            return video_info

    # YOUTUBE забираем id видео
    def find_id(url):
        if (url.find("/embed/")>0):
            return Youtube.get_embed_id(url)
        video_id_pos = url.find("?v=") + 3
        video_id = url[video_id_pos:]
        video_id = video_id.rstrip()
        print(video_id)
        return video_id

    def get_embed_id(url):
        _from = url.find("/embed/") + 7
        _to = url.find("?f")
        video_id = url[_from:]
        print(video_id)
        return video_id

    #https://www.youtube.com/watch?v=-C34I8_CPo4
    def live_video(id):
        url = "https://www.youtube.com/get_video_info?&video_id="+id+"&el=info&ps=default&eurl=&gl=US&hl=en"
        responce = requests.get(url).text
        dec_responce = urllib.parse.parse_qs(responce)
        #print(dec_responce)
        try:
            m3u8_url = str(dec_responce['hlsvp'])[2:-2]
        except:
            return False
        #print(m3u8_url)
        responce = requests.get(m3u8_url).text
        m3u8_obj = m3u8.loads(responce)
        list = {}
        for p in m3u8_obj.playlists:
            if (str(p.stream_info.resolution) == "(1920, 1080)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Youtube.video_id + " : " + "STREAM" + " : " + "1080"] = p.uri
            if (str(p.stream_info.resolution) == "(1280, 720)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Youtube.video_id + " : " + "STREAM" + " : " + "720"] = p.uri
            if (str(p.stream_info.resolution) == "(854, 480)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Youtube.video_id + " : " + "STREAM" + " : " + "480"] = p.uri
            if (str(p.stream_info.resolution) == "(640, 360)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Youtube.video_id + " : " + "STREAM" + " : " + "360"] = p.uri
            if (str(p.stream_info.resolution) == "(426, 240)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Youtube.video_id + " : " + "STREAM" + " : " + "240"] = p.uri
            if (str(p.stream_info.resolution) == "(256, 144)"):
                size, audio = FFredact.FFredact.ff_redact(p.uri)
                print(size, audio)
                list[Youtube.video_id + " : " + "STREAM" + " : " + "144"] = p.uri
        print(list)
        return list

    # YOUTUBE получаем строку из урлов
    def find_video_url(video_code):
        video_code = video_code.strip()
        url = "http://www.youtube.com/get_video_info?video_id=" + str(video_code)
        responce = requests.get(url).text
        dec_responce = urllib.parse.parse_qs(responce)
        try:
            return dec_responce['url_encoded_fmt_stream_map'][0].split(",")
        except KeyError:
            return("empty")


    # YOUTUBE раскодируем все урлы
    def dec_urls(urls):
        result = []
        for url in urls:
            result.append(urllib.parse.parse_qs(url))
        return result

    #получить словарь ссылок
    def get_links_list(info):
        url = info[0]['url'][0]
        links = {}
        for n in info:
            size, audio = FFredact.FFredact.ff_redact(n['url'])
            print(size, audio)
            s = Youtube.video_id + " : "+Youtube.itag_normalize(str(n['itag']))
            links[s] = n['url']
        print(links)
        return links

    # YOUTUBE загрузка
    def get_json(links):
        dump = json.dumps(links)
        json_doc = json.loads(dump)
        return (json_doc)

    #перевод itag кода к нормальному виду
    def itag_normalize(itag):
        itag = itag[2:-2]
        if itag == "5":
            return "FLV : 240"
        if itag == "34":
            return "FLV : 360"
        if itag == "35":
            return "FLV : 480"
        if itag == "18":
            return "MP4 : 360"
        if itag == "22":
            return "MP4 : 720"
        if itag == "37":
            return "MP4 : 1080"
        if itag == "38":
            return "MP4 : 4K"
        if itag == "43":
            return "WEBM : 360"
        if itag == "44":
            return "WEBM : 480"
        if itag == "45":
            return "WEBM : 720"
        if itag == "36":
            return "3GP : 240"
        if itag == "17":
            return "3GP : 144"
        else:
            return itag