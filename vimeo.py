import requests
import json
import m3u8
import FFredact

class Vimeo:

    video_id = ""

    def run(url):
        link = Vimeo.get_link(url)
        list = Vimeo.get_list(link)
        if not list:
            return False
        else:
            return list

    #получить ссылку на json
    def get_link(url):
        if (url.find("player.vimeo.com")>0):
            pos = url.find("video/")+6
            id = url[pos:]
            url = "https://vimeo.com/"+id
        url = url.strip()
        Vimeo.video_id = url[url.find("com/") + 4:]
        responce = requests.get(url).text
        pos = responce.find('"GET","https://player.vimeo.com') + 7
        #pos1 = responce.find(",!0")-1
        pos1 = pos + 300
        link = str(responce)[pos: pos1]
        pos1 = link.find(",!0") - 1
        link = link[:pos1]
        print(link)
        return link

    #получить список ссылок
    def get_list(link):
        try:
            responce = requests.get(link).json()
        except:
            return False
        l = {}
        index = 1
        for n in responce['request']['files']['dash']['cdns']:
            link= responce['request']['files']['dash']['cdns'][n]['url']
            l[Vimeo.video_id + " : " + "DASH" + " : " + str(index)] = link
            index = index+1

        for n in responce['request']['files']['hls']['cdns']:
            link = responce['request']['files']['hls']['cdns'][n]['url']
            url = str(link)
            m3u8_list = Vimeo.get_m3u8_list(url)
            l.update(m3u8_list)
            index = index + 1

        for n in responce['request']['files']['progressive']:
            quality = n['height']
            link = n['url']
            #print(link)
            size, audio = FFredact.FFredact.ff_redact(link)
            l[Vimeo.video_id + " : " + "MP4" + " : " + str(size)] = link
        print(l)
        return l

    def get_json(links):
        dump = json.dumps(links)
        json_doc = json.loads(dump)
        return (json_doc)

    def get_m3u8_list(url):
        l = {}
        responce = requests.get(url).text
        m3u8_obj = m3u8.loads(responce)
        pos = url.rfind("/")
        url = url[:pos]
        pos = url.rfind("/")
        url = url[:pos]
        for p in m3u8_obj.playlists:
            a = str(p.uri)[2:]
            #print(url+a)
            size, audio = FFredact.FFredact.ff_redact(url+a)
            #print(size, audio, url+a)
            l[str(Vimeo.video_id) + " : " + "STREAM" + " : " + str(size)] = url+a
        return l