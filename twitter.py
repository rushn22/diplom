import urllib
import json
from bs4 import BeautifulSoup
import requests
import m3u8
import FFredact

class Twitter:

    video_id = ""

    def run(url):
        link = Twitter.get_videolink(url)
        if not link:
            return False
        pos = link.find("videos/")+7
        pos1 = link.find("?")
        Twitter.video_id = link[pos: pos1]
        m3u8_obj = Twitter.get_m3u8(link)
        list = Twitter.get_list(m3u8_obj)
        if not list:
            return False
        else:
            return list

    #получаем ссылку на само видео
    def get_videolink(url):
        try:
            data = urllib.request.urlopen(url).read()
        except:
            return False
        soup = BeautifulSoup(data, "html.parser")
        #print(soup.title)
        link = ''
        for v in soup.find_all('meta'):  # берем оттуда все meta
            src = v.get('property')
            if (str(src)=="og:video:url"):
                link = v.get('content')
                print(link)
        if not link:
            return False
        return link

    #получаем m3u8 файл
    def get_m3u8(link):
        data = urllib.request.urlopen(link).read()
        #print(data)
        pos = str(data).find("video_url")
        m3u8_link = str(data)[pos: pos+150]
        pos = m3u8_link.find("https")
        pos1 = m3u8_link.find("m3u8")+4
        m3u8_link = m3u8_link[pos: pos1]
        m3u8_link = m3u8_link.replace("\\\\","")
        print(m3u8_link)
        responce = requests.get(m3u8_link).text
        m3u8_obj = m3u8.loads(responce)
        return m3u8_obj

    #делаем плейлист в список
    def get_list(m3u8_obj):
        list = {}
        for p in m3u8_obj.playlists:
            if (str(p.stream_info.resolution) == "(320, 180)"):
                url = "https://video.twimg.com"+p.uri
                size,audio = FFredact.FFredact.ff_redact(url)
                print(size,audio)
                list[Twitter.video_id + " : " + "MP4" + " : " + size] = "https://video.twimg.com"+p.uri
            if (str(p.stream_info.resolution) == "(640, 360)"):
                url = "https://video.twimg.com" + p.uri
                size, audio = FFredact.FFredact.ff_redact(url)
                print(size, audio)
                list[Twitter.video_id + " : " + "MP4" + " : " + size] = "https://video.twimg.com"+p.uri
        print(list)
        if not list:
            return False
        return list

    def get_json(links):
        dump = json.dumps(links)
        json_doc = json.loads(dump)
        return (json_doc)