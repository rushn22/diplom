import requests
import json
import m3u8
import subprocess as sp
import FFredact

class Onetv:

    json_api = "http://json-api.1internet.tv/1tv-json-api/projects/video?id={id}"
    def run(url):
        url = url.strip()
        if url=="http://www.1tv.ru/live" or url == "http://www.1tv.ru/live/":
            m3u8_url = "http://mobdrm.1tv.ru/hls-live10/streams/1tv/1tv1.m3u8?cdn=cdn6.1internet.tv&s=SRjfQnSqPiRjleM%2F3KQTDiKk2wwKitap7wFqyhYL%2F4Chiu9dEgZhLcyUUOqKBa0dSPcy1epKulRigp1YNhmyYpGSDHQHiLfJUnoPSOpwT2rh7EngWT9ddoldkGtpaZmwKIEkVkcASXmENHH8meR26YG%2BKmXIOr8%2BkFdzaXiiFah8C8pN%2FVd6vyit7bdCfb3RaswRTTX7KZJOY5eiFlO7qqgKtsaH3v5AILC7Y8wlDeM%3D"
            a = {}
            a["1tv : STREAM : 360"] = m3u8_url
            #print(a)
            return a
        id = Onetv.get_id(url)
        list = Onetv.parse_json(id)
        if not list:
            return False
        else:
            return list

    def get_id(url):
        url = url.strip()
        responce = requests.get(url).text
        pos = responce.find("project/logo/")+13
        pos1 = pos + 50
        id = responce[pos: pos1]
        id = id[id.find("/")+1: id.find(".png")+4]
        id = id[id.find("/")+1: id.find(".png")]
        id = id[: id.find("_")]
        print(str(id))
        return id

    def parse_json(self):
        Onetv.json_api = Onetv.json_api.replace("{id}",self)
        print(Onetv.json_api)
        responce = requests.get(Onetv.json_api).json()
        l = {}
        for n in responce['videos']:
            video_name = n['video_name']
            for p in n['source']:
                if (p['type'] == "application/x-mpegURL"):
                    #l[video_name+ " : " + "STREAM" + " : " + "480"] = p['src']
                    link = str(p['src'])
                    responce = requests.get(link.strip()).text
                    m3u8_obj = m3u8.loads(responce)
                    for k in m3u8_obj.playlists:
                        if (str(k.stream_info.resolution) == "(640, 360)"):
                            pos = p['src'].rfind("/")
                            p['src'] = str(p['src'])[:pos+1]
                            playlist_url = p['src'] + k.uri
                            size,audio = FFredact.FFredact.ff_redact(playlist_url)
                            print("Обработка видео")
                            l[video_name + " : " + "STREAM" + " : " + str(size)] = playlist_url
                    #print(p['src'])
                if (p['type'] == "video/mp4"):
                    size,audio = FFredact.FFredact.ff_redact(p['src'])
                    print("Обработка видео")
                    l[video_name + " : " + "MP4" + " : " + size] = p['src']
                    #print(p['src'])
        #print(l)
        if not l:
            return False
        else:
            return l

    def get_json(links):
        dump = json.dumps(links)
        json_doc = json.loads(dump)
        return(json_doc)