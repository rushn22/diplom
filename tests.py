import unittest
import sys

import youtube
import vkontakte
import rutube
import twitch
import vimeo
import dailymotion
import ustream
import onetv
import facebook
import twitter
import iframe_parse
import tldextract

class TestMethods(unittest.TestCase):

    TXT_NAME = ""
    l = []
    def setUp(self):
        if TestMethods.TXT_NAME != "":
            print(TestMethods.TXT_NAME)
            f  = open(TestMethods.TXT_NAME, "r")
            for line in f.readlines():
                TestMethods.l.append(line.strip())
            print(TestMethods.l)
        else:
            TestMethods.l.append("https://www.youtube.com/watch?v=KWp6mcZiFEw")
            TestMethods.l.append("https://vk.com/videos52533513?z=video52533513_169123008%2Fpl_52533513_-2")
            TestMethods.l.append("https://m.vk.com/video52533513_169123008")
            TestMethods.l.append("https://rutube.ru/video/217ff81fa38247ed280386ef7a1514a6/")
            #TestStringMethods.l.append("https://www.twitch.tv/igromania")
            #TestMethods.l.append("https://www.1tv.ru/shows/kvn/vypuski/tretya-igra-sezona-vysshaya-liga-kvn-vypusk-ot-26-03-2017")
            TestMethods.l.append("https://vimeo.com/177185122")
            #TestStringMethods.l.append("http://www.dailymotion.com/video/x5go71u_valerian-and-the-city-of-a-thousand-planets-new-teaser-trailer_news")
            #TestMethods.l.append("http://www.ustream.tv/decoraheagles")
            TestMethods.l.append("https://www.facebook.com/ShamovD/videos/1432743677024935/")
            TestMethods.l.append("https://twitter.com/UpdatesHarryUK/status/854282578790162432")
            TestMethods.l.append("https://html5book.ru/html5-video/")
            TestMethods.l.append("http://anidub.com/anime/novosti-anime-industrii/tokijskij-gul-tokyo-ghoul-2017-pervyj-trejler/")
            TestMethods.l.append("http://www.html5tutorial.info/html5-audio.php")


    def test_print(self):
        for n in TestMethods.l:
            ext = tldextract.extract(n)
            if (ext.domain == "youtube"):
                self.assertTrue(youtube.Youtube.run(n))
                continue
            if (ext.domain == "vk"):
                self.assertTrue(vkontakte.VK.run(n))
                continue
            if (ext.domain == "rutube"):
                self.assertTrue(rutube.Rutube.run(n))
                continue
            if (ext.domain == "twitch"):
                self.assertTrue(twitch.Twitch.run(n))
                continue
            if (ext.domain == "1tv"):
                self.assertTrue(onetv.Onetv.run(n))
                continue
            if (ext.domain == "vimeo"):
                self.assertTrue(vimeo.Vimeo.run(n))
                continue
            if (ext.domain == "dailymotion"):
                self.assertTrue(dailymotion.Dailymotion.run(n))
                continue
            if (ext.domain == "ustream"):
                self.assertTrue(ustream.Ustream.run(n))
                continue
            if (ext.domain == "facebook"):
                self.assertTrue(facebook.Facebook.run(n))
                continue
            if (ext.domain == "twitter"):
                self.assertTrue(twitter.Twitter.run(n))
                continue
            else:
                self.assertTrue(iframe_parse.Iframe.get_iframe_src(n))
if __name__ == '__main__':
    if len(sys.argv) > 0:
        TestMethods.TXT_NAME = sys.argv.pop()
    unittest.main()
