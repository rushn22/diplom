import requests
import json
import tldextract
from bs4 import BeautifulSoup

import youtube
import vkontakte
import rutube
import twitch
import vimeo
import dailymotion
import ustream

class Iframe:

    #получить все src из iframe
    def get_iframe_src(url):
        responce = requests.get(url).text
        links = []
        soup = BeautifulSoup(responce, "html.parser")
        links = Iframe.iframe_tag(soup,links)
        links = Iframe.media_tag(url, soup, links, 'video')
        links = Iframe.media_tag(url, soup, links, 'audio')
        return links

    def iframe_tag(soup, links):
        iframes = soup.findAll('iframe')  # находим тег <iframe>
        #print(iframes)
        for n in iframes:  # забираем все src в список
            links.append(n.get('src'))
        return links

    def media_tag(url, soup, links, media):
        video = soup.findAll(media)  # находим тег
        #print(video)
        soup = BeautifulSoup(str(video), "html.parser")
        source = soup.findAll('source')

        #нормализуем ссылку
        if (url[-1:] != "/"):
            if url[-4:] == "html" or url[-3:] == "php":
                pos = url.rfind("/")
                url = url[:pos]
            url = url + "/"

        for n in source:  # забираем все src в список
            src = n.get('src')
            if (src[:1] == "/"):
                src = src[1:]
            link = url + src
            print(link)
            links.append(link)
        return links

    #сделать json файл из обработанных видео, если обработать не удается, то забить
    def get_json(links):
        l = {}
        for link in links:
            ext = tldextract.extract(link)
            if (ext.domain == "youtube"):
                print(link)
                try:
                    video_info = youtube.Youtube.run(link)
                    l.update(youtube.Youtube.get_links_list(video_info))
                except Exception:
                    print("Невозможно обработать ссылку")
                    print("----------------")
            if (ext.domain == "vk"):
                print(link)
                try:
                    video_link = vkontakte.VK.run(link)
                    l.update(vkontakte.VK.get_list(video_link))
                except Exception:
                    print("Невозможно обработать ссылку")
                    print("----------------")
            if (ext.domain == "rutube"):
                print(link)
                try:
                    l.update(rutube.Rutube.run(link))
                except Exception:
                    print("Невозможно обработать ссылку")
                    print("----------------")
            if (ext.domain == "twitch"):
                print(link)
                try:
                    m3u8_obj = twitch.Twitch.run(link)
                    l.update(twitch.Twitch.get_list(m3u8_obj))
                except Exception:
                    print("Невозможно обработать ссылку")
                    print("----------------")
            if (ext.domain == "vimeo"):
                print(link)
                try:
                    l.update(vimeo.Vimeo.run(link))
                except Exception:
                    print("Невозможно обработать ссылку")
                    print("----------------")
            if (ext.domain == "dailymotion"):
                print(link)
                try:
                    l.update(dailymotion.Dailymotion.run(link))
                except Exception:
                    print("Невозможно обработать ссылку")
                    print("----------------")
            if (ext.domain == "ustream"):
                print(link)
                try:
                    l.update(ustream.Ustream.run(link))
                except Exception:
                    print("Невозможно обработать ссылку")
                    print("----------------")
            else:
                print(link)
                pos = link.rfind(".")+1
                pos1 = link.rfind("/")+1
                name = link[pos1:pos-1]
                media_format = link[pos:]
                print(name,media_format)
                l[name + " : " + media_format + " : " + "unknown"] = link
        if not l:
            return False
        dump = json.dumps(l)
        json_doc = json.loads(dump)
        #print(json_doc)
        return json_doc
