import urllib
import json
import FFredact

class Facebook:

    video_id = ""

    def run(url):
        Facebook.get_id(url)
        data = Facebook.get_data(url)
        videolink = Facebook.get_videolink(data)
        if not videolink:
            return False
        else:
            audiolink = Facebook.get_audiolink(data)
            return Facebook.get_list(videolink, audiolink)

    def get_id(url):
        pos = url.find("videos/")+7
        id = url[pos:]
        id = id.replace("/","")
        print(id)
        Facebook.video_id = id
        return

#https://www.facebook.com/ShamovD/videos/1432743677024935/
    def get_data(url):
        data = urllib.request.urlopen(url).read()
        #print(data)
        return data

    #получить ссылку на видео
    def get_videolink(data):
        #получаем контейнер со ссылками
        pos = str(data).find("[{video:")
        pos1 = str(data).find("audio:[{")
        links = str(data)[pos + 9: pos1 - 2]
        #получаем ссылку на видео
        pos = links.find('url:"') + 5
        pos1 = links.find('",start')
        link = links[pos: pos1]
        length = len(link)
        if (length > 300):
            return False
        return link

    def get_audiolink(data):
        #получаем контейнер со ссылками
        pos = str(data).find('audio:[{')
        links = str(data)[pos + 6: pos + 256]
        # получаем ссылку на видео
        pos = links.find('url:"') + 5
        pos1 = links.find('",st')
        link = links[pos: pos1]
        return link

    def get_list(video, audio):
        m = {}
        print(video)
        print(audio)
        size,sound = FFredact.FFredact.ff_redact(video)
        print(size,sound)
        m[Facebook.video_id + " : MP4 : "+size] = video
        size, sound = FFredact.FFredact.ff_redact(audio)
        print(size, sound)
        m[Facebook.video_id + " : MP3 : " + sound] = audio
        #print(m)
        return m

    def get_json(links):
        dump = json.dumps(links)
        json_doc = json.loads(dump)
        return(json_doc)