import subprocess as sp

class FFredact:

    def ff_redact(url):
        FFMPEG_BIN = "ffmpeg"
        command = [FFMPEG_BIN, '-i', url]
        pipe = sp.Popen(command, stdout=sp.PIPE, stderr=sp.PIPE)
        pipe.stdout.readline()
        pipe.terminate()
        infos = pipe.stderr.read()

        ff_video = 0
        ff_audio = 0
        infos = str(infos).split('\\r\\n')
        for n in infos:
            if n.find("Video:") > 0:
                ff_video = n
            if n.find("Audio:")>0:
                ff_audio = n
        #print(ff_video)
        #print(ff_audio)
        size = 0
        if ff_video != 0:
            pos = ff_video.find("p, ")
            if (pos == -1):
                pos = ff_video.find("p(tv), ")+4
            if (pos == 3):
                pos = ff_video.find("p(tv, bt709), ")+11
            if (pos == 10):
                pos = ff_video.find("p(tv, smpte170m),")+15
            if (pos == 14):
                pos = ff_video.find("p(progressive), ")+13
            size = ff_video[pos+3:]
            pos1 = size.find(",")
            size = size[:pos1]
            #print(size)
            size = size + " " + FFredact.get_bitrate(ff_video)
            size = size.strip()
        if ff_audio != 0:
            bitrate = FFredact.get_bitrate(ff_audio)
            if (bitrate!=""):
                ff_audio = bitrate
        return size, ff_audio

    def get_bitrate(string):
        pos = string.find("kb/s")
        bitrate = ""
        if pos>0:
            bitrate = string[pos-5:pos+4]
            bitrate = bitrate.strip()
        return bitrate