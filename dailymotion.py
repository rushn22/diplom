import requests
import json
import m3u8
import FFredact

class Dailymotion:

    video_id = ""

    def run(url):
        bad_link = Dailymotion.get_link(url)
        link = Dailymotion.normalize_link(bad_link)
        m3u8file = Dailymotion.get_m3u8(link)
        if not m3u8file:
            return False
        else:
            return Dailymotion.get_list(m3u8file)

    def get_link(url):
        if (url.find("embed/video/")>0):
            pos = url.find("embed/video/")+12
            Dailymotion.video_id = url[pos:]
            url = "https://www.dailymotion.com/video/"+Dailymotion.video_id
        responce = requests.get(url).text
        pos = responce.find('"stream_chromecast_url":"')+25
        pos1 = pos + 200
        link = responce[pos:pos1]
        pos1= link.find('"partner":true')-2
        link = link[:pos1]
        print(link)
        return link

    def normalize_link(link):
        link = link.replace("\\","")
        print(link)
        return link


    def get_m3u8(link):
        try:
            responce = requests.get(link).text
        except:
            return False
        print(responce)
        m3u8_obj = m3u8.loads(responce)
        return m3u8_obj

    def get_list(m3u8_obj):
        list = {}
        for p in m3u8_obj.playlists:
            size,audio = FFredact.FFredact.ff_redact(p.uri)
            list[Dailymotion.video_id + " : " + "MP4" + " : "+ size]=p.uri
        print(list)

    def get_json(links):
        dump = json.dumps(links)
        json_doc = json.loads(dump)
        return (json_doc)