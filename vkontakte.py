import urllib
import urllib.parse
import json
from bs4 import BeautifulSoup
from urllib.request import urlopen
#from grab import Grab
import FFredact

class VK:

    video_id = ""

    def run(url):
        key = VK.get_id(url)
        if (key == "empty"):
            return False
        link = VK.get_link(key)
        links = VK.get_list(link)
        return links

    #получить ID видео
    def get_id(url):
        if (url.find("video_ext")>0):
            return VK.get_from_embed(url)
        if (url.find("m.vk.com")>0):
            return VK.get_m_id(url)
        responce = url
        dec_responce = urllib.parse.parse_qs(responce)
        try:
            key = str(list(dec_responce.values())[0])  # параметры ссылки
        except IndexError:
            return("empty")
        pos = key.find("video") + 5
        key = key[pos:-2].strip()  # все записи после video
        pos = key.find("/")
        key = key[0:pos]  # избавляемся от лишнего хвоста
        pos = key.find("_")
        user_id = key[0:pos]
        video_id = key[pos + 1:]
        VK.video_id = key
        return key

    #распарсим src с iframe
    def get_from_embed(url):
        pos = url.find("oid=")+4
        pos1 = url.find("&id")
        key = url[pos:pos1]
        pos = url.find("&hash")
        key = key + "_" + url[pos1+4:pos]
        return key

    #обработка мобильной версии
    def get_m_id(url):
        pos = url.find("_") + 1
        VK.video_id = url[pos:].strip()
        pos = url.find("video")+5
        key = url[pos:]
        return key

    #получить прямую ссылку на видео
    def get_link(key):
        url = 'http://m.vk.com/video' + str(key)
        print(url)
        responce = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(responce, "html.parser")
        video = soup.find('video')  # находим тег <video>
        src = ''
        for v in video.find_all('source'):  # берем оттуда все source
            src = v.get('src')  # берем из source src ссылку
        return src

    #проверка существует ли разрешение видео
    def check_status(link):
        try:
            r = urllib.request.urlopen(link)
            code = str(r.getcode())
        except:
            code = "0"
        return code

    #делаем из ссылки на 240 ссылки на другие разрешения
    def get_list(a):
        m = {}
        m[VK.video_id + ":" + "MP4-240"] = a
        link = str(a).replace('.240.', '.360.')
        code = VK.check_status(link)
        if code == "200":
            size,audio= FFredact.FFredact.ff_redact(link)
            #print(size,audio)
            m[VK.video_id + ":" + "MP4 : " + str(size)] = link

        link = str(a).replace('.240.', '.480.')
        code = VK.check_status(link)
        if code == "200":
            size, audio = FFredact.FFredact.ff_redact(link)
            m[VK.video_id + ":" + "MP4 : " + str(size)] = link

        link = str(a).replace('.240.', '.720.')
        code = VK.check_status(link)
        if code == "200":
            size, audio = FFredact.FFredact.ff_redact(link)
            m[VK.video_id + ":" + "MP4 : " + str(size)] = link
        print(m)
        return m

    #делаем из списка ссылок Json документ
    def get_json(links):
        dump = json.dumps(links)
        json_doc = json.loads(dump)
        return(json_doc)
