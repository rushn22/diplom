import requests     #pip install requests
import urllib
import urllib.parse
import m3u8         #https://pypi.python.org/pypi/m3u8/0.2.4
import json
import tldextract   #https://pypi.python.org/pypi/tldextract#downloads
from bs4 import BeautifulSoup   #pip install beautifulsoup4
from ffmpy import FFmpeg    #пока не используется
from urllib.request import urlopen

import youtube
import vkontakte
import rutube
import iframe_parse
import twitch
import onetv
import vimeo
import dailymotion
import ustream
import facebook
import twitter


def main():
    url = input('URL: ')
    url = url.strip()
    ext = tldextract.extract(url)
    print(ext.domain)

    # пример: https://www.youtube.com/watch?v=KWp6mcZiFEw
    if(ext.domain == "youtube"):
        links=youtube.Youtube.run(url)
        if links:
            return youtube.Youtube.get_json(links)
        else:
            return False


    # пример: https://vk.com/videos52533513?z=video52533513_169123008%2Fpl_52533513_-2
    if (ext.domain == "vk"):
        links = vkontakte.VK.run(url)
        if links:
            return vkontakte.VK.get_json(links)
        else:
            return False

    #https://rutube.ru/video/217ff81fa38247ed280386ef7a1514a6/
    if (ext.domain == "rutube"):
        list = rutube.Rutube.run(url)
        if list:
            return rutube.Rutube.get_playlists_json(list)
        else:
            return False

    #https://www.twitch.tv/igromania
    if (ext.domain == "twitch"):
        links = twitch.Twitch.run(url)
        if links:
            return twitch.Twitch.get_json(links)
        else:
            return False

    #https://www.1tv.ru/shows/kvn/vypuski/tretya-igra-sezona-vysshaya-liga-kvn-vypusk-ot-26-03-2017
    if (ext.domain == "1tv"):
        list = onetv.Onetv.run(url)
        if list:
            return onetv.Onetv.get_json(list)
        else:
            return False

    # https://vimeo.com/177185122
    if (ext.domain == "vimeo"):
        list = vimeo.Vimeo.run(url)
        if list:
            return vimeo.Vimeo.get_json(list)
        else:
            return False

    ##http://www.dailymotion.com/video/x5jkugh_taron-egerton-channing-tatum-halle-berry-in-kingsman-the-golden-circle-trailer-1_news
    if (ext.domain == "dailymotion"):
        list = dailymotion.Dailymotion.run(url)
        if list:
            return dailymotion.Dailymotion.get_json(list)
        else:
            return False

    #http://www.ustream.tv/decoraheagles
    if (ext.domain == "ustream"):
        list = ustream.Ustream.run(url)
        if list:
            return ustream.Ustream.get_json(list)
        else:
            return False

    # https://www.facebook.com/ShamovD/videos/1432743677024935/
    if (ext.domain == "facebook"):
        list = facebook.Facebook.run(url)
        if list:
            return facebook.Facebook.get_json(list)
        else:
            return False

    # https://twitter.com/UpdatesHarryUK/status/854282578790162432
    if (ext.domain == "twitter"):
        list = twitter.Twitter.run(url)
        if list:
            return twitter.Twitter.get_json(list)
        else:
            return False
    else:
        links = iframe_parse.Iframe.get_iframe_src(url.strip())
        return iframe_parse.Iframe.get_json(links)
main()