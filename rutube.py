import urllib
import urllib.parse
import json
from bs4 import BeautifulSoup
from urllib.request import urlopen
import FFredact

class Rutube:

    video_id = ""

    def run(url):
        Rutube.video_id = Rutube.get_id(url)
        m3u8_block = Rutube.get_m3u8_block(Rutube.video_id)
        if not m3u8_block:
            return False
        else:
            return Rutube.get_playlists(m3u8_block)

    #получить id видео
    def get_id(url):
        if(url.find("embed/")>0):
            return Rutube.get_from_embed(url)
        pos = str(url).find("rutube.ru/video/") + 16
        print(pos)
        video_id = url.strip()[pos:]  # забрали id видео
        if video_id.find("/")>0:    #вдруг остался слэш в конце
            video_id = video_id[:-1]
        print(video_id)
        return video_id

    #обработка src с embed
    def get_from_embed(url):
        if url.find("?p")>0:
            pos = url.find("embed/")+6
            pos1 = url.find("?p")
            Rutube.video_id = url[pos:pos1]
        else:
            Rutube.video_id = url[url.find("embed/")+6:].strip()
        return Rutube.video_id

    #получить m3u8 файл с плейлистами по резрешениям
    def get_m3u8_block(video_id):
        url = "http://rutube.ru/api/play/options/" + str(video_id) + "/?format=xml"
        try:
            responce = urllib.request.urlopen(url).read()  # код страницы
        except:
            return False
        soup = BeautifulSoup(responce, "html.parser")
        m3u8_block = soup.find('m3u8')  # находим тег <video>
        m3u8_block = str(m3u8_block)
        m3u8_block = m3u8_block[6:-7].strip()
        m3u8_block = m3u8_block.replace("amp;", "")  # ссылка на m3u8
        #print(m3u8_block)

        responce = urllib.request.urlopen(m3u8_block).read()
        responce = str(responce)[2:-1].replace("\\n", " ")  # делаем файл красивым
        #print(responce)
        return responce

    #получить ссылки на плейлисты с разрешениями
    def get_playlists(responce):
        resp_list = str(responce).split(" ")  # делаем из документа список из элементов
        list_of_links = []
        for n in resp_list:  # составляем список из ссылок на плейлист из ts
            if n.find(".m3u8") > 0:
                list_of_links.append(n)
        m = {}
        for n in list_of_links:  # ссылки в json разрешение:ссылка
            size,audio = FFredact.FFredact.ff_redact(n)
            print(size,audio)
            #s = Rutube.video_id+":"+Rutube.itag_normalize(" "+n[n.find("?i=") + 3:])
            s = Rutube.video_id+" : MP4 : "+ size
            m[s] = n
        print(m)
        return m

    def get_playlists_json(list):
        dump = json.dumps(list)
        json_doc = json.loads(dump)
        return json_doc

    #сделать нормальные названия
    def itag_normalize(itag):
        if (str(itag).find("640x360")>0):
            return "MP4 : 360"
        if (str(itag).find("512x288")>0):
            return "MP4 : 288"
        if (str(itag).find("896x504")>0):
            return "MP4 : 504"
        if (str(itag).find("1280x720")>0):
            return "MP4 : 720"
        else:
            return itag